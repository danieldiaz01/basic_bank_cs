# README #
Actividad numero 4 , realizar un banco (con carencia seguridad)  que siga el modelo 
C-S con 5 operaciones basicas: 

- Transferencia entre cuentas
- Consultar saldo
- Depositar 
- Retirar
- Crear usuario

Leyenda:

- add [user] : Añade usuario  (primera operacion que se deberia hacer)
- tns [user_fuente] [user_destino] [monto] : transfiere de un monto posible acuentas existentes
- dep [user] [monto]  : deposita una cantidad cualquiera a cualquier usuario existente
- con [user] : consulta el saldo de cualquier usuario existente.
- ret [user] [monto] : Retira un monto existente de cualquier usuario , si este existe. 

primero compilar con el make y correr el servidor 
para el cliente ingresar la solicitud junto con el ejecutable 
ej:

./client add [user] 