/* Cliente Banco ::::::::::::::::::::::::::::::::::::::::: 

Actividad numero 4 , realizar un banco (con carencia seguridad)  que siga el modelo 
C-S con 4 operaciones basicas: 

- Transferencia entre cuentas
- Consultar saldo
- Depositar 
- Retirar
- Crear usuario

Leyenda:

- add [user] : Añade usuario  (primera operacion que se deberia hacer)
- tns [user_fuente] [user_destino] [monto] : transfiere de un monto posible acuentas existentes
- dep [user] [monto]  : deposita una cantidad cualquiera a cualquier usuario existente
- con [user] : consulta el saldo de cualquier usuario existente.
- ret [user] [monto] : Retira un monto existente de cualquier usuario , si este existe. 

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */ 

#include <czmq.h>
#include <string>
#include <iostream>

using namespace std;


int main (int argc, char ** argv)
{
    
    zctx_t *context = zctx_new (); 
    void *requester = zsocket_new(context, ZMQ_REQ); 
    zsocket_connect (requester, "tcp://localhost:5555");  
    zmsg_t* request= zmsg_new(); 
    
    if(strcmp(argv[1], "add")==0){     // Creamos usuario  
    
        zmsg_addstr(request,"ADD");
        zmsg_addstr(request,argv[2]);    
            
    }else if(strcmp(argv[1],"tns")==0){
            zmsg_addstr(request,"TNS");
            zmsg_addstr(request,argv[2]); // Usuario fuente .
            zmsg_addstr(request,argv[3]); // Usuario destino.
            zmsg_addstr(request,argv[4]); // Monto a pasar entre cuentas.
    }else if(strcmp(argv[1],"dep")==0){
            zmsg_addstr(request,"DEP");
            zmsg_addstr(request,argv[2]); // Usuario a depositar saldo.
            zmsg_addstr(request,argv[3]); // Saldo a depositar.           
    }else if(strcmp(argv[1],"con")==0){
            zmsg_addstr(request,"CON");
            zmsg_addstr(request,argv[2]);  // Usuario a consultar saldo.             
     }else if(strcmp(argv[1],"ret")==0){
            zmsg_addstr(request,"RET");
            zmsg_addstr(request,argv[2]); // usuario a retirar. 
            zmsg_addstr(request,argv[3]); // monto.              
            }          
     else{ cout<<"Operacion no valida"<<endl; return 0; } 
  
    zmsg_send(&request,requester);
    zmsg_t* resp=zmsg_recv(requester);
    zmsg_print(resp); 
    zctx_destroy(&context);

    return 0;
}



