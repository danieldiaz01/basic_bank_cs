# This is a variable
CC=g++ -std=c++11




all: client server

client: client.cc
	$(CC)  -c client.cc
	$(CC) -o client client.o -lzmq -lczmq

server: server.cc
	$(CC)  -c server.cc
	$(CC)  -o server server.o -lzmq -lczmq

clean:
	rm -f client.o server.o client server *~

#/home/utp/Descargas
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/utp/cs/zmq/lib


#$ LD_LIBRARY_PATH=/usr/local/lib
#$ export LD_LIBRARY_PATH
#Now try running your command.