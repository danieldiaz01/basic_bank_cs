//  SERVIDOR BANCO 
#include <czmq.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <unordered_map>
#include <string>
#include <cassert>
#include <iostream>


using namespace std;
typedef unordered_map<string,int> BankType;
BankType base;

void dispatch(zmsg_t* incmsg,zmsg_t* outmsg){
    zmsg_print(incmsg);
    string operation(zmsg_popstr(incmsg));
    string id(zmsg_popstr(incmsg));
    if(operation.compare("ADD")==0){
        if(base.count(id)>0){            
            zmsg_addstr(outmsg,"El usuario ya existe!!!");
        }else{  
            base[id]=0; // creamos nuevo usuario con su id y 0 de saldo.     
            zmsg_addstr(outmsg,"Usuario creado con exito");                    
        }        
    }else if(operation.compare("CON")==0){
        if(base.count(id)>0){            
            int saldo=base[id];
            string stsaldo = to_string(saldo);
            zmsg_addstr(outmsg,"El saldo es :  ");
            zmsg_addstr(outmsg,stsaldo.c_str());     
        }else{                
            zmsg_addstr(outmsg,"El usuario no existe!!!");                   
        }
     }else if(operation.compare("DEP")==0){
        if(base.count(id)>0){   
            string nuevo(zmsg_popstr(incmsg));         
            int a= base[id];
            int b= stoi(nuevo);
            int total = a+b;
            base[id]=total;
            string stsaldo = to_string(total);
            zmsg_addstr(outmsg,"Se han depositado exitosamente : ");
            zmsg_addstr(outmsg,nuevo.c_str());
            zmsg_addstr(outmsg,"Saldo total : ");
            zmsg_addstr(outmsg,stsaldo.c_str());     
        }else{                
            zmsg_addstr(outmsg,"El usuario no existe!!!");                   
        }

        }else if(operation.compare("RET")==0){
        if(base.count(id)>0){   
            string cantidad(zmsg_popstr(incmsg));         
            int a = base[id];
            int b= stoi(cantidad);
            int total = a-b;
            if (a<b){
            zmsg_addstr(outmsg,"No se puede retirar esa cantidad !!! ");                
            }else{
            base[id]=total;
            string stsaldo = to_string(total);
            zmsg_addstr(outmsg,"Se han retirado exitosamente :  ");
            zmsg_addstr(outmsg,cantidad.c_str());
            }             
        }else{                
            zmsg_addstr(outmsg,"El usuario no existe!!!");                   
        }

        }else if(operation.compare("TNS")==0){           
        if(base.count(id)>0){
            int total=base[id];      
            string id2(zmsg_popstr(incmsg));       
            string cantidad(zmsg_popstr(incmsg));         
            int a= base[id];
            int b= stoi(cantidad);            
            if (a<b){
            zmsg_addstr(outmsg,"No se puede Transferir esa cantidad !!! ");
            }else{total = a-b;

            if(base.count(id2)>0)
            { // esperamos a que existan las 2 cuentas
            base[id]=total; // reacomodamos la cuenta del cotizante.
            int c=base[id2]; 
            base[id2]=b+c; // reacomodamos la cuenta del beneficiario.
            string stsaldo = to_string(total);
            zmsg_addstr(outmsg,"Se ha Transferido exitosamente !!! Saldo restante");
            zmsg_addstr(outmsg,stsaldo.c_str());
            }else{zmsg_addstr(outmsg,"No existe la cuenta destino !!! ");}
            }
           
            
            }else{zmsg_addstr(outmsg,"El usuario fuente no existe!!!");}

        }else{zmsg_addstr(outmsg,"ERROR!");}
}
  

int main (void)
{   cout<<"::::::::::::: SERVIDOR BANCO INSEGURITO I.N.C - Confiando en lo inconfiable ::::::::::::"<<endl;
    zctx_t *context = zctx_new ();
    void *responder = zsocket_new (context, ZMQ_REP); 
    zsocket_bind (responder, "tcp://*:5555"); 
    
    while (1) {
        zmsg_t* incmsg=zmsg_new();
        zmsg_t* outmsg=zmsg_new();

        incmsg=zmsg_recv(responder);        
        dispatch(incmsg,outmsg);
        zmsg_send(&outmsg,responder);
        
    }
    return 0;
}
